LIGO/Virgo Documentation Index
==============================

Welcome to the LIGO/Virgo documentation index! Here you can find documentation
for a number of projects of the LIGO Scientific Collaboration and the Virgo
Collaboration.

Manuals
-------

* `LIGO/Virgo Public Alerts User Guide </userguide>`_

Projects
--------

* `gwcelery </gwcelery>`_: LIGO/Virgo public alert pipeline
* `ligo-gracedb </ligo-gracedb>`_: GraceDB client
